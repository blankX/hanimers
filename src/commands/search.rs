use crate::utils;

use clap::ArgMatches;
use reqwest::Client;
use std::process::exit;

pub const AVALIABLE_TAGS: &[&str] = &[
    "3D",
    "Ahegao",
    "Anal",
    "BDSM",
    "Big Boobs",
    "Blow Job",
    "Bondage",
    "Boob Job",
    "Censored",
    "Comedy",
    "Cosplay",
    "Creampie",
    "Dark Skin",
    "Facial",
    "Fantasy",
    "Filmed",
    "Foot Job",
    "Futanari",
    "Gangbang",
    "Glasses",
    "Hand Job",
    "Harem",
    "HD",
    "Horror",
    "Incest",
    "Inflation",
    "Lactation",
    "Loli",
    "Maid",
    "Masturbation",
    "Milf",
    "Mind Break",
    "Mind Control",
    "Monster",
    "Nekomimi",
    "NTR",
    "Nurse",
    "Orgy",
    "Plot",
    "POV",
    "Pregnant",
    "Public Sex",
    "Rape",
    "Reverse Rape",
    "Rimjob",
    "Scat",
    "School Girl",
    "Shota",
    "Softcore",
    "Swimsuit",
    "Teacher",
    "Tentacle",
    "Threesome",
    "Toys",
    "Trap",
    "Tsundere",
    "Ugly Bastard",
    "Uncensored",
    "Vanilla",
    "Virgin",
    "Watersports",
    "X-Ray",
    "Yaoi",
    "Yuri",
];

pub async fn search(arg_m: &ArgMatches) {
    let broad_search = arg_m.is_present("broad");
    let tags = arg_m.values_of("tags").unwrap_or_default().collect();
    let query = arg_m
        .values_of("query")
        .unwrap_or_default()
        .collect::<Vec<_>>()
        .join(" ");
    let query = query.trim();
    let results = utils::search(Client::new(), query, tags, broad_search)
        .await
        .unwrap();
    if results.is_empty() {
        eprintln!("No results found");
        exit(1);
    }
    for i in results {
        println!("{}", i);
    }
}
