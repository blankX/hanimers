use crate::utils;

use clap::ArgMatches;
use reqwest::redirect::Policy;
use reqwest::Client;
use std::process::exit;
extern crate tokio;

pub async fn view(arg_m: &ArgMatches) {
    let policy = Policy::custom(|attempt| {
        if attempt.previous().len() > 10 {
            attempt.error("too many redirects")
        } else if attempt.url().path() == "/404" {
            attempt.stop()
        } else {
            attempt.follow()
        }
    });
    let client = Client::builder().redirect(policy).build().unwrap();
    let handles = arg_m
        .values_of("id")
        .unwrap()
        .map(|id| {
            let cloned_client = client.clone();
            let id = id.to_string();
            let cid = id.clone();
            (
                tokio::spawn(async move { utils::get_hentai(cloned_client, &cid).await }),
                id,
            )
        })
        .collect::<Vec<_>>();
    let mut fail = false;
    let mut one_done = false;
    for handle in handles {
        let (handle, id) = handle;
        let hentai = match handle.await {
            Ok(hentai) => hentai,
            Err(err) => {
                if one_done {
                    eprintln!("");
                }
                eprintln!("ID: {}\nError: {}", id, err);
                fail = true;
                one_done = true;
                continue;
            }
        };
        match hentai {
            Ok(hentai) => {
                match hentai {
                    Some(hentai) => {
                        if one_done {
                            println!("");
                        }
                        println!("{}", &hentai);
                    }
                    None => {
                        if one_done {
                            eprintln!("");
                        }
                        eprintln!("ID: {}\nError: does not exist", id);
                        fail = true;
                    }
                };
            }
            Err(err) => {
                if one_done {
                    eprintln!("");
                }
                eprintln!("ID: {}\nError: {}", id, err);
                fail = true;
            }
        };
        one_done = true;
    }
    if fail {
        exit(1);
    }
}
