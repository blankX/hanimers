mod commands;
mod structs;
mod unescape;
mod utils;
use clap::{Arg, Command};
extern crate tokio;

fn main() {
    let matches = Command::new("hanimers")
        .about("hanime.tv downloader in rust")
        .version(env!("CARGO_PKG_VERSION"))
        .subcommand_required(true)
        .subcommand(
            Command::new("search")
            .arg(
                Arg::new("tags")
                    .long("tags")
                    .short('t')
                    .takes_value(true)
                    .use_value_delimiter(true)
                    .ignore_case(true)
                    .possible_values(commands::AVALIABLE_TAGS)
            ).arg(
                Arg::new("broad")
                    .long("broad")
                    .short('b')
                    .help("More results, but less accurate. Videos will match if they contain any selected tag rather than all selected tags.")
            ).arg(
                Arg::new("query")
                    .takes_value(true)
                    .multiple_values(true)
                    .help("Search query")
            )
        )
        .subcommand(
            Command::new("view")
            .visible_aliases(&["info", "show"])
            .arg(
                Arg::new("id")
                    .takes_value(true)
                    .multiple_values(true)
                    .required(true)
            )
        )
        .subcommand(
            Command::new("download")
            .visible_alias("dl")
            .arg(
                Arg::new("print")
                    .long("print")
                    .short('p')
                    .help("Print the URL to download only")
            ).arg(
                Arg::new("resolution")
                    .long("resolution")
                    .short('r')
                    .help("Set preferred resolution")
                    .takes_value(true)
            ).arg(
                Arg::new("id")
                    .takes_value(true)
                    .multiple_values(true)
                    .required(true)
            )
        )
        .get_matches();

    let runtime = tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build()
        .unwrap();
    match matches.subcommand() {
        Some(("search", sub_m)) => runtime.block_on(commands::search(sub_m)),
        Some(("view", sub_m)) => runtime.block_on(commands::view(sub_m)),
        Some(("download", sub_m)) => runtime.block_on(commands::download(sub_m)),
        _ => unreachable!("subcommand_required do your job please"),
    };
}
